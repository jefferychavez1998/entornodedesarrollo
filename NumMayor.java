import java.util.Scanner;

public class NumMayor{
	public static void main(String[] args)
	{
		int mayor = 0;
		Scanner sc = new Scanner(System.in);
		System.out.println("Ingrese el primer numero:");
		int num1 = sc.nextInt();
		System.out.println("Ingrese el segundo numero:");
		int num2 = sc.nextInt();
		System.out.println("Ingrese el tercer numero:");
		int num3 = sc.nextInt();
		
		mayor = (num1>num2)?num1:num2;
		mayor = (mayor>num3)?mayor:num3;
		
		System.out.println("El numero mayor es: "+mayor);

		
		mayor = (num1>num2)?(num1>num3?num1:num3):(num2>num3?num2:num3);
		
		System.out.println("El numero mayor es: "+mayor);

	}
}